function InputHintCreator(domElements){
  this.label = domElements.label;
  this.searchInput = domElements.searchInput;
}

InputHintCreator.prototype.setValOfSearchInput = function(){
  this.searchInput.val(this.label.text());
}

InputHintCreator.prototype.addClassToSearchInput = function(){
  this.searchInput.addClass("hint");
}

InputHintCreator.prototype.removeLabel = function(){
  this.label.remove();
}

InputHintCreator.prototype.bindFocusEvent = function(){
  var _this = this;
  this.searchInput.on("focus", function(){
    $(this).removeClass("hint").val("");
  });
}

InputHintCreator.prototype.bindBlurEvent = function(){
  var _this = this;
  this.searchInput.on("blur", function(){
    var thisSearchInput = $(this);
    if (thisSearchInput.val().trim() === ""){
      thisSearchInput
      .val(_this.label.text())
      .addClass("hint");
    }
  });
};

InputHintCreator.prototype.init = function(){
  this.setValOfSearchInput();
  this.addClassToSearchInput();
  this.removeLabel();
  this.bindFocusEvent();
  this.bindBlurEvent();
};

$(document).ready(function(){
  var form = $("form#search");

  var domElements = {
    label: form.find("label"),
    searchInput: form.find("input[type='text']")
  };

  var inputHintCreator = new InputHintCreator(domElements);
  inputHintCreator.init();
});